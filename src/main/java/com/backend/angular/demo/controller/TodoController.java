package com.backend.angular.demo.controller;

import com.backend.angular.demo.model.Todo;
import com.backend.angular.demo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TodoController {

    @Autowired
    private TodoService todoService;

    @RequestMapping("/todoName")
    public String hello(){
        return todoService.getTodoName();
    }

    @RequestMapping(value = "/addTodo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Todo> addTodo(@RequestBody Todo todo){
        if(todo.getId() == null){
            todoService.saveTodoAsJsonFile(todo);
        } else {
            todoService.editTodo(todo);
        }

        return todoService.getTodos();
    }

    @ResponseBody
    @RequestMapping(value = "/getAllTodo", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Todo> getAllTodos(){
        return todoService.getTodos();
    }

    @RequestMapping(value = "/editTodo",  method = RequestMethod.PUT)
    public String editTodo(@RequestBody Todo todo){
        todoService.editTodo(todo);

        return "Todo updated";
    }

    @RequestMapping(value = "/getTodo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Todo getTodoById(Long id){
        return todoService.findById(id);
    }

}
