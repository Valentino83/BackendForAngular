package com.backend.angular.demo.service;

import com.backend.angular.demo.model.Todo;

import java.util.List;

public interface TodoService {

    String getTodoName();

    void saveTodoAsJsonFile(Todo todo);

    List<Todo> getTodos();

    void editTodo(Todo dodo);

    Todo findById(Long id);

}
