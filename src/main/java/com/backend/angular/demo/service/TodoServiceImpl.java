package com.backend.angular.demo.service;

import com.backend.angular.demo.model.Todo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

@Service
public class TodoServiceImpl implements TodoService {

    public static final String JSON_FILE_PATH = "C:\\Angular\\temp\\todoList.json";
    public static final String LAST_ID_FILE_PATH = "C:\\Angular\\temp\\last_id.txt";

    public String getTodoName(){
        return "Todo name";
    }

    @Override
    public void saveTodoAsJsonFile(Todo todo){
        todo.setId(getLastId());
        ObjectMapper mapper = new ObjectMapper();
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(JSON_FILE_PATH, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        try {
            printWriter.println(mapper.writeValueAsString(todo));  //New line
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        printWriter.close();
        setLastId(todo.getId() + 1);
    }

    @Override
    public List<Todo> getTodos(){
        List<Todo> list = new ArrayList<Todo>();
        ObjectMapper mapper = new ObjectMapper();
        try {
            Scanner scanner = new Scanner(new File(JSON_FILE_PATH));
            while (scanner.hasNextLine()) {
                Todo todo = mapper.readValue(scanner.nextLine(), Todo.class);
                list.add(todo);
            }
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }

    @Override
    public void editTodo(Todo todo){
        List<Todo> todoList = new ArrayList<Todo>();
        ObjectMapper mapper = new ObjectMapper();
        try {
            Scanner scanner = new Scanner(new File(JSON_FILE_PATH));
            while (scanner.hasNextLine()) {
                Todo todoFromFile = mapper.readValue(scanner.nextLine(), Todo.class);
                if(todoFromFile.getId() != null && todoFromFile.getId().equals(todo.getId())){
                    todoFromFile.setName(todo.getName());
                    todoFromFile.setWeekDay(todo.getWeekDay());
                    todoFromFile.setDone(todo.isDone());
                }
                todoList.add(todoFromFile);
            }
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(JSON_FILE_PATH, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        todoList.forEach( t -> {
            try {
                printWriter.println(mapper.writeValueAsString(t));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });

        printWriter.close();
    }

    @Override
    public Todo findById(Long id){
        ObjectMapper mapper = new ObjectMapper();
        Todo todo = null;
        try {
            Scanner scanner = new Scanner(new File(JSON_FILE_PATH));
            while (scanner.hasNextLine()) {
                Todo t = mapper.readValue(scanner.nextLine(), Todo.class);
                if(t.getId().equals(id)){
                   todo = t;
                   break;
                }
            }
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return todo;
    }

    private Long getLastId(){
        Long lastId = -1L;
        try {
            Scanner scanner = new Scanner(new File(LAST_ID_FILE_PATH));
            String line = scanner.nextLine();
            lastId  = !StringUtils.isEmpty(line)?Long.valueOf(line):0;
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchElementException e){
            lastId = 0L;
        }

        return lastId;
    }

    private void setLastId(Long id){
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(LAST_ID_FILE_PATH, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.print(id);
        printWriter.close();
    }

}
