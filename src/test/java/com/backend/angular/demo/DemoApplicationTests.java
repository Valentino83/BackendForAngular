package com.backend.angular.demo;

import com.backend.angular.demo.model.Todo;
import com.backend.angular.demo.service.TodoService;
import com.backend.angular.demo.service.TodoServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.backend.angular.demo.service.TodoServiceImpl.JSON_FILE_PATH;

@SpringBootTest
class DemoApplicationTests {

    private TodoService todoService = new TodoServiceImpl();

	@Test
	void checkFolder() {
        Todo todo = new Todo();
        todo.setName("Todo name test");
        todo.setWeekDay("Monday");
        todoService.saveTodoAsJsonFile(todo);
        Assert.isTrue(new File(JSON_FILE_PATH).exists(), "new json file should exist");
	}

	@Test
    void  getAllTodos(){
        Todo todo = new Todo();
        todo.setName("Todo name test");
        todo.setWeekDay("Monday");
        todoService.saveTodoAsJsonFile(todo);
        todo.setName("Todo name test");
        todo.setWeekDay("Tuesday");
        todoService.saveTodoAsJsonFile(todo);

        List<Todo> todos = todoService.getTodos();
        Assert.isTrue(todos != null, "todos is not null");
        Assert.isTrue(todos.size() > 1, "toods are existing");
    }

    @Test
    void testEditTodo(){
        Todo todo = new Todo();
        todo.setName("First todo test");
        todo.setWeekDay("Tuesday");
        todoService.saveTodoAsJsonFile(todo);
        todo.setName("Edit todo name");
        todo.setId(0L);
        todoService.editTodo(todo);

        ObjectMapper mapper = new ObjectMapper();
        boolean found = false;
        try {
            Scanner scanner = new Scanner(new File(JSON_FILE_PATH));
            while (scanner.hasNextLine()) {
                Todo todoFromFile = mapper.readValue(scanner.nextLine(), Todo.class);
                if(todoFromFile.getId() != null && todoFromFile.getId().equals(todo.getId())){
                    found = true;
                }
            }
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Assert.isTrue(found, "todo has been updated");
    }

}
